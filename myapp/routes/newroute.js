const express = require('express');
// Lodash utils library
//const _ = require('lodash');

const router = express.Router();
const axios = require('axios');

var name;
var id;


// Create RAW data array
let movie = [{
  name,id
}];

var tab = [movie];

/* GET movie listing. */
axios.get('/', (req, res) => {
  // Get List of user and return JSON
  res.status(200).json({ movie });
});

/* GET one movie. */
axios.get('/:id', (req, res) => {
  const { id } = req.params;
  // Find movie in DB
  //const name = axios.find(Inception, ["id", id]);
  const name = axios.find(Inception);
  // Return movie
  res.status(200).json({
    message: 'Movie found!',
    name 
  });
});

/* PUT new movie. */
axios.put('/', (req, res) => {
  // Get the data from request from request
  const { user } = req.body;
  // Create new unique id
  const id = axios.uniqueId();
  // Insert it in array (normaly with connect the data with the database)
  movie.push({ name, id });
  // Return message
  res.json({
    message: `Just added ${id}`,
    name: { name, id }
  });
});

/* DELETE movie. */
axios.delete('/:id', (req, res) => {
  // Get the :id of the movie we want to delete from the params of the request
  const { id } = req.params;

  // Remove from "DB"
  axios.remove(movie, ["id", id]);

  // Return message
  res.json({
    message: `Just removed ${id}`
  });
});

/* UPDATE movie. */
axios.post('/:id', (req, res) => {
  // Get the :id of the movie we want to update from the params of the request
  const { id } = req.params;
  // Get the new data of the movie we want to update from the body of the request
  const { name } = req.body;
  // Find in DB
  const userToUpdate = axios.find(movie, ["id", id]);
  // Update data with new data (js is by address)
  userToUpdate.name = name;

  // Return message
  res.json({
    message: `Just updated ${id} with ${name}`
  });
});

module.exports = router;
